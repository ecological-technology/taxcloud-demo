# taxcloud-demo 代码说明

#### 介绍
1、taxcloud-demo是基于springboot的税务云接口测试案例，在test下涵盖了大部分税务云OpenAPI的测试数据，可直接启动运行，需要了解整个接口调用逻辑debug一下即可。  
2、可以结合税务云接口测试工具使用,位置/resources/tools/tax-cloud-api-tool.jar（cmd 运行java -jar tax-cloud-api-tool.jar命令，可启动测试小工具。访问地址：http://192.168.1.9:8081/）

#### 版本要求
springboot 2.x  
maven 3.x  
jdk 1.8  

#### 使用说明
1.  打开项目
2.  在test启动对应测试类方法即可

#### 配置文件介绍
| 包名         | 说明                  |
| ---------- | -------------------    | 
| taxcloud-*.properties | 税务云不同环境的配置           | 
| url.properties | 税务云OpenAPI的url配置         |
| application.properties    | 项目配置，修改spring.profiles.active值可切换到对应环境   | 

#### 包名介绍
| 包名         | 说明                  |
| ---------- | -------------------    | 
| config | 存放模块相关配置类           | 
| controller        | 存放回调接收服务类        | 
| crypto | 存放签名验签的算法类         |
| param        | 存放请求参数构造类        | 
| service        | 存放Service相关类   | 
| utils | 存放模块相关工具类 | 
| vo        | 存放模块相关实体类        | 


#### 主要类名介绍
| 类名         | 说明                  |
| ---------- | -------------------    | 
| InputTaxUrlConfig.java | 税务云受票URL配置读取类           | 
| OutputTaxUrlConfig.java | 税务云开票URL配置读取类         |
| TaxCloudConfig.java        | 税务云环境配置类   | 
| JwtParamBuilder.java | 签名验签参数 | 
| SignHelper.java        | 签名的工具类        |
| ITaxCloudAPIToolService.java |  税务云api测试工具业务类 | 
| APIToolVO.java | 税务云测试工具VO | 
| BillCollectionsTest.java | 报销台账接口测试样例 | 
| PurchaseCollectionTest.java | 采购台账接口测试样例 | 
| InputLedgerTest.java | 进项业务台账、票据中心接口测试样例 | 
| InvoiceApplyTest.java | 开票申请相关接口测试样例 |
| InvoiceWillTest.java | 未开票收入管理相关接口测试样例 |
| RedinfoApplyTest.java | 红字信息表相关接口测试样例 |
| SignTest.java | 签名连通接口测试样例 |
| CallbackController.java | 开票接收回调服务样例 |
| DingDingPiaoedaController.java | 钉钉对接个人票夹相关接口样例 |
| EnterpriseWechatPiaoedaController.java | 企业微信对接个人票夹相关接口样例 |

#### 新增接口测试样例流程
1. 配置url.properties
2. 在config下的url配置类中新增属性
3. 在test下编写测试样例即可

