package com.yonyou.eco.taxclouddemo;

import com.yonyou.eco.taxclouddemo.config.PiaoedaWebUrlConfig;
import com.yonyou.eco.taxclouddemo.param.input_ticket.StaBookBuildParam;
import com.yonyou.eco.taxclouddemo.service.ITaxCloudAPIToolService;
import com.yonyou.eco.taxclouddemo.utils.Base64Util;
import com.yonyou.eco.taxclouddemo.vo.APIToolVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * 功能描述: 其它接口测试
 * @Author: jiaoguojin
 * @Date: 2023/3/8 11:02
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class OtherTest {

    @Autowired
    APIToolVO apiToolVO;

    @Autowired
    PiaoedaWebUrlConfig piaoedaWebUrlConfig;

    @Autowired
    ITaxCloudAPIToolService toolService;

    //下载票据中心接口
    @Test
    public void queryvatbilllist() {
        apiToolVO.setUrl("/input-tax/api/bill/queryvatbilllist");
        //构造POST表单Map
        Map<String, Object> paramsMap = new HashMap<>();
        Map<String, Object> searchParamMap = new HashMap<>();
        searchParamMap.put("orgcode","1");
        searchParamMap.put("billDateBegin","2023-03-01");
        searchParamMap.put("billDateEnd","2023-03-14");
        paramsMap.put("searchParam",searchParamMap);
        paramsMap.put("pagesize", 900);
        paramsMap.put("pagenum",1);
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

}
