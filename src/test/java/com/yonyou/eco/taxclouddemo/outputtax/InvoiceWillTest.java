package com.yonyou.eco.taxclouddemo.outputtax;

import com.yonyou.eco.taxclouddemo.config.OutputTaxUrlConfig;
import com.yonyou.eco.taxclouddemo.param.output_invoice.InvoiceWill;
import com.yonyou.eco.taxclouddemo.service.ITaxCloudAPIToolService;
import com.yonyou.eco.taxclouddemo.vo.APIToolVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

/**
 * 功能描述: 未开票收入管理
 * @Author: jiaoguojin
 * @Date: 2023/3/8 11:03
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class InvoiceWillTest {

    @Autowired
    APIToolVO apiToolVO;

    @Autowired
    OutputTaxUrlConfig outputTaxUrlConfig;

    @Autowired
    ITaxCloudAPIToolService toolService;

    /*
     * 未开票查询
     */
    @Test
    public void result() {
        apiToolVO.setUrl(outputTaxUrlConfig.getResult());
        Map<String, Object> paramsMap = InvoiceWill.result();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 未开票管理新增单据
     */
    @Test
    public void save() {
        apiToolVO.setUrl(outputTaxUrlConfig.getSave());
        Map<String, Object> paramsMap = InvoiceWill.save();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 未开票记录变更查询
     */
    @Test
    public void changes() {
        apiToolVO.setUrl(outputTaxUrlConfig.getChanges());
        Map<String, Object> paramsMap = InvoiceWill.change();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

}
