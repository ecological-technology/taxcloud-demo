package com.yonyou.eco.taxclouddemo.outputtax;

import com.alibaba.fastjson.JSON;
import com.yonyou.eco.taxclouddemo.config.OutputTaxUrlConfig;
import com.yonyou.eco.taxclouddemo.param.output_invoice.InvoiceBuildParam;
import com.yonyou.eco.taxclouddemo.service.ITaxCloudAPIToolService;
import com.yonyou.eco.taxclouddemo.utils.Sm4Util;
import com.yonyou.eco.taxclouddemo.vo.APIToolVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;
import java.util.UUID;

/**
 * 功能描述: 开票申请相关
 * @Author: jiaoguojin
 * @Date: 2023/3/8 11:03
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class InvoiceApplyTest {

    @Autowired
    APIToolVO apiToolVO;

    @Autowired
    OutputTaxUrlConfig outputTaxUrlConfig;

    @Autowired
    ITaxCloudAPIToolService toolService;

    /*
     * 开蓝票
     */
    @Test
    public void insertWithArray() {
        apiToolVO.setUrl(outputTaxUrlConfig.getInsertWithArray());
        //构造POST表单Map
        Map<String, Object> paramsMap = InvoiceBuildParam.buildInsertWithArrayPostParam();//数电传参说明看此处
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByForm(apiToolVO);
    }

    /*
     * 开蓝票-自动拆分
     */
    @Test
    public void insertWithSplit() {
        apiToolVO.setUrl(outputTaxUrlConfig.getInsertWithSplit());
        Map<String, Object> paramsMap = InvoiceBuildParam.insertWithSplit();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByForm(apiToolVO);
    }

    /*
     * 开票状态查询
     */
    @Test
    public void queryInvoiceStatus() {
        apiToolVO.setUrl(outputTaxUrlConfig.getQueryInvoiceStatus());
        //构造POST表单Map
        Map<String, String> paramsMap = InvoiceBuildParam.buildQueryInvoiceStatusPostParam();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByForm(apiToolVO);
    }

    /*
     * 发票红冲请求
     */
    @Test
    public void red() {
        apiToolVO.setUrl(outputTaxUrlConfig.getRed());
        Map<String, Object> paramsMap = InvoiceBuildParam.red();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByForm(apiToolVO);
    }

    /*
     * 开票申请审核通过
     */
    @Test
    public void issue() {
        apiToolVO.setUrl(outputTaxUrlConfig.getIssue());
        Map<String, Object> paramsMap = InvoiceBuildParam.issue();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByForm(apiToolVO);
    }


    /*
     * 发票部分红冲
     */
    @Test
    public void part_red() {
        apiToolVO.setUrl(outputTaxUrlConfig.getPart_red());
        Map<String, Object> paramsMap = InvoiceBuildParam.partRed();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByForm(apiToolVO);
    }

    /*
     * 删除开票失败申请
     */
    @Test
    public void deleteInvoiceFailData() {
        apiToolVO.setUrl(outputTaxUrlConfig.getDeleteInvoiceFailData());
        Map<String, Object> paramsMap = InvoiceBuildParam.delete();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByForm(apiToolVO);
    }

    /*
     * 重发邮件
     */
    @Test
    public void callBackByEmail() {
        apiToolVO.setUrl(outputTaxUrlConfig.getCallBackByEmail());
        Map<String, Object> paramsMap = InvoiceBuildParam.emaillCallBack();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByForm(apiToolVO);
    }

    /*
     * 开票申请删除
     */
    @Test
    public void del() {
        apiToolVO.setUrl(outputTaxUrlConfig.getDel());
        Map<String, Object> paramsMap = InvoiceBuildParam.buildInvoiceApplyDelParam();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByForm(apiToolVO);
    }

    /*
     * 发票打印
     */
    @Test
    public void print() {
        apiToolVO.setUrl(outputTaxUrlConfig.getPrint());
        Map<String, Object> paramsMap = InvoiceBuildParam.buildTaxPrintParam();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 请求二维码信息
     */
    @Test
    public void insertForQRInvoice() {
        apiToolVO.setUrl(outputTaxUrlConfig.getInsertForQRInvoice());
        //构造POST表单Map
        Map<String, Object> paramsMap = InvoiceBuildParam.buildRequestDatasQR();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }


    /*
     * 纸质发票作废
     */
    @Test
    public void invalid() {
        apiToolVO.setUrl(outputTaxUrlConfig.getInvalid());
        Map<String, Object> paramMap = InvoiceBuildParam.buildInvalidParam();
        apiToolVO.setRequestMap(paramMap);
        toolService.postByForm(apiToolVO);
    }

    /*
     * SM4加密解密测试
     */
    @Test
    public void sm4test() throws Exception {
        System.out.println("开始测试SM4加密解密====================");
        //要加密的数据
        String username = "aaaa";
        String password = "123";
        //销售方纳税人识别号
        String nsrsbh = "91370213MA3M26CP0C";
        //将销售方纳税人识别号转成32位uuid做为秘钥key
        String key = UUID.nameUUIDFromBytes(nsrsbh.getBytes()).toString().replaceAll("-","");
        //国密四加密用户名
        String cipherusername = Sm4Util.encryptEcb(key,username);//sm4加密
        //国密四加密密码
        String  cipherpassword = Sm4Util.encryptEcb(key,password);//sm4加密
        System.out.println("加密后用户名："+cipherusername);
        System.out.println("加密后密码："+cipherpassword);
        System.out.println("——————————————————————————————");
        System.out.println("解密后用户名："+Sm4Util.decryptEcb(key,"5d74e909c44246eb8c08b8b60285d9ce"));
        System.out.println("解密后密码："+Sm4Util.decryptEcb(key,"c96d02aaa5315c2ed11dca2bd99b694e"));
        System.out.println("结束===================");
    }
}
