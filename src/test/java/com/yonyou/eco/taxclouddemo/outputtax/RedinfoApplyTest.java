package com.yonyou.eco.taxclouddemo.outputtax;

import com.yonyou.eco.taxclouddemo.config.OutputTaxUrlConfig;
import com.yonyou.eco.taxclouddemo.param.output_invoice.InvoiceWill;
import com.yonyou.eco.taxclouddemo.service.ITaxCloudAPIToolService;
import com.yonyou.eco.taxclouddemo.vo.APIToolVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * 功能描述: 红字信息表相关接口
 * @Author: jiaoguojin
 * @Date: 2023/3/8 11:02
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedinfoApplyTest {

    @Autowired
    APIToolVO apiToolVO;

    @Autowired
    OutputTaxUrlConfig outputTaxUrlConfig;

    @Autowired
    ITaxCloudAPIToolService toolService;

    /*
     * 申请红字信息表（购方销方）
     */
    @Test
    public void insertWithRedApply() {
        apiToolVO.setUrl(outputTaxUrlConfig.getInsertWithRedApply());
        Map<String, Object> paramsMap = InvoiceWill.apply();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 查询红字信息表编号
     */
    @Test
    public void queryRedInfoApply() {
        apiToolVO.setUrl(outputTaxUrlConfig.getQueryRedInfoApply()+"/1387992599455621120");
        toolService.get(apiToolVO);
    }
}
