package com.yonyou.eco.taxclouddemo.sign;

import com.yonyou.eco.taxclouddemo.config.OutputTaxUrlConfig;
import com.yonyou.eco.taxclouddemo.service.ITaxCloudAPIToolService;
import com.yonyou.eco.taxclouddemo.vo.APIToolVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 功能描述: 签名连通测试
 * @Author: jiaoguojin
 * @Date: 2023/3/8 11:03
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SignTest {

    @Autowired
    APIToolVO apiToolVO;

    @Autowired
    OutputTaxUrlConfig outputTaxUrlConfig;

    @Autowired
    ITaxCloudAPIToolService toolService;

    /*
     * 签名验签
     */
    @Test
    public void testconnection() {
        apiToolVO.setUrl(outputTaxUrlConfig.getTestconnection());
        apiToolVO.setRequestBody("{\n" +
                "    \"nsrsbh\": \"201609140000001\",\n" +
                "    \"orgcode\": \"20160914001\"\n" +
                "}");
        toolService.postByJson(apiToolVO);
    }
}
