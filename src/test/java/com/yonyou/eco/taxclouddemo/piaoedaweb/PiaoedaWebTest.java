package com.yonyou.eco.taxclouddemo.piaoedaweb;

import com.yonyou.eco.taxclouddemo.config.PiaoedaWebUrlConfig;
import com.yonyou.eco.taxclouddemo.param.input_ticket.StaBookBuildParam;
import com.yonyou.eco.taxclouddemo.service.ITaxCloudAPIToolService;
import com.yonyou.eco.taxclouddemo.utils.Base64Util;
import com.yonyou.eco.taxclouddemo.vo.APIToolVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * 功能描述: 个人票夹接口测试样例
 * @Author: jiaoguojin
 * @Date: 2023/3/8 11:02
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PiaoedaWebTest {

    @Autowired
    APIToolVO apiToolVO;

    @Autowired
    PiaoedaWebUrlConfig piaoedaWebUrlConfig;

    @Autowired
    ITaxCloudAPIToolService toolService;

    //OCR识别接口
    @Test
    public void rqrecognise() {
        apiToolVO.setUrl(piaoedaWebUrlConfig.getRqrecognise());
        //构造POST表单Map
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("usermobile","15011181852");
        paramsMap.put("useremail","");
        paramsMap.put("file", Base64Util.imageToBase64("C:\\Users\\Administrator\\Desktop\\c51e2d5a3e70d084d34175dc5ff558b.jpg"));
        paramsMap.put("fileName","c51e2d5a3e70d084d34175dc5ff558b.jpg");
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    //OCR识别接口V2
    @Test
    public void v2_rqrecognise() {
        apiToolVO.setUrl(piaoedaWebUrlConfig.getV2_rqrecognise());
        //构造POST表单Map
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("usermobile","18611143356");
        paramsMap.put("useremail","");
        paramsMap.put("file", Base64Util.imageToBase64("D:\\常用文件\\02税务云\\测试数据\\图片\\增值税发票1.png"));
        paramsMap.put("fileName","增值税发票1.png");
        paramsMap.put("imageCut","1");
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    //OCR接口图片预览接口
    //直接浏览器访问url即可 例如：https://yesfp.yonyoucloud.com/piaoeda-web/api/bill/ocr/preview?imageId=1472108354232672256
    @Test
    public void ocr_preview() {
        apiToolVO.setUrl(piaoedaWebUrlConfig.getOcr_preview());
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("imageId","1472108354232672256");
        apiToolVO.setRequestMap(paramsMap);
        toolService.get(apiToolVO);
    }

    /**
     * 个人票夹列表查询
     */
    @Test
    public void query() {
        apiToolVO.setUrl(piaoedaWebUrlConfig.getQuery()+"?page=1&size=10");
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.buildInfo();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    //个人票夹票据详情查询
    @Test
    public void detail()  {
        apiToolVO.setUrl(piaoedaWebUrlConfig.getDetail());
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.detial();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    //修改发票状态
    @Test
    public void purchaserStatus()  {
        apiToolVO.setUrl(piaoedaWebUrlConfig.getPurchaser_status());
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.changFapiaoStatus();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    //根据号码代码获取信息
    @Test
    public void summar()  {
        apiToolVO.setUrl(piaoedaWebUrlConfig.getSummary());
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.summar();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    //附件预览 以base64返回
    @Test
    public void perview()  {
        apiToolVO.setUrl(piaoedaWebUrlConfig.getPreview());
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.detial();;
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }


    /**
     * 个人票夹附件下载   以附件二进制流数据返回
     */
    @Test
    public void download() {
        apiToolVO.setUrl(piaoedaWebUrlConfig.getDownload());
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.detial();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    //个人票夹新增
    @Test
    public void add() {
        apiToolVO.setUrl(piaoedaWebUrlConfig.getAdd());
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.add();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }


    //个人票夹删除
    @Test
    public void billDelete() {
        apiToolVO.setUrl(piaoedaWebUrlConfig.getDelete());
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.billDelete();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    //个人票夹修改
    @Test
    public void billUpdate() {
        apiToolVO.setUrl(piaoedaWebUrlConfig.getUpdate());
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.billUpdate();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    //个人票夹提交发票到报销台账_全票种
    @Test
    public void commit() {
        apiToolVO.setUrl(piaoedaWebUrlConfig.getCommit());
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.commit();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    //个人票夹行程单预览
    @Test
    public void preview() {
        apiToolVO.setUrl(piaoedaWebUrlConfig.getItinerary_preview());
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.view();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    //个人票夹行程单下载
    @Test
    public void itineraryDownload() {
        apiToolVO.setUrl(piaoedaWebUrlConfig.getItinerary_download());
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.view();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    //个人票夹发票提交到采购台账
    @Test
    public void fetch() {
        apiToolVO.setUrl(piaoedaWebUrlConfig.getFetchPurchase());
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.fetch();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

}
