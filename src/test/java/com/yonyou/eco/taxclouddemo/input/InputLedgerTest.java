package com.yonyou.eco.taxclouddemo.input;

import com.yonyou.eco.taxclouddemo.config.InputLedgerUrlConfig;
import com.yonyou.eco.taxclouddemo.config.InputTaxUrlConfig;
import com.yonyou.eco.taxclouddemo.param.input_ledger.InputLedgerParam;
import com.yonyou.eco.taxclouddemo.param.input_ticket.PurchaseParam;
import com.yonyou.eco.taxclouddemo.param.input_ticket.StaBookBuildParam;
import com.yonyou.eco.taxclouddemo.param.personal_ticket_holder.ReimburseCollection;
import com.yonyou.eco.taxclouddemo.service.ITaxCloudAPIToolService;
import com.yonyou.eco.taxclouddemo.vo.APIToolVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 功能描述: 进项业务台账、票据中心接口测试样例
 * @Author: jiaoguojin
 * @Date: 2023/3/8 15:39
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class InputLedgerTest {

    @Autowired
    APIToolVO apiToolVO;

    @Autowired
    InputLedgerUrlConfig inputLedgerUrlConfig;

    @Autowired
    ITaxCloudAPIToolService toolService;

    /*
     * 保存到进项电子票据中心
     */
    @Test
    public void saveonlytobillcenter() {
        apiToolVO.setUrl(inputLedgerUrlConfig.getSaveonlytobillcenter());
        //构造POST表单Map
        Map<String, Object> paramsMap = InputLedgerParam.buildSaveonlytobillcenter();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 保存电子票据中心的票据到台账
     */
    @Test
    public void saveonlybusicollection() {
        apiToolVO.setUrl(inputLedgerUrlConfig.getSaveonlybusicollection());
        //构造POST表单Map
        Map<String, Object> paramsMap = InputLedgerParam.buildSaveonlybusicollection();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 按号码代码查询票据信息
     */
    @Test
    public void query() {
        apiToolVO.setUrl(inputLedgerUrlConfig.getQuery());
        //构造POST表单Map
        List list = new ArrayList();
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("fpDm","011002300211");
        paramsMap.put("fpHm","59978095");
        paramsMap.put("billType","invoice");
        list.add(paramsMap);
        apiToolVO.setRequestMap(list);
        toolService.postByJsonList(apiToolVO);
    }

    /*
     * 进项电子票据查询-增值税发票
     */
    @Test
    public void queryvatbilllist() {
        apiToolVO.setUrl(inputLedgerUrlConfig.getQueryvatbilllist());
        //构造POST表单Map
        Map<String, Object> paramsMap = InputLedgerParam.buildQueryvatbilllist();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 删除台账（不包含票据删除）
     */
    @Test
    public void delonlybusicollection() {
        apiToolVO.setUrl(inputLedgerUrlConfig.getDelonlybusicollection());
        //构造POST表单Map
        Map<String, Object> paramsMap = InputLedgerParam.buildDelonlybusicollection();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 完成报账(支持按来源id报账)
     */
    @Test
    public void reimbursedv2() {
        apiToolVO.setUrl(inputLedgerUrlConfig.getReimbursedv2());
        //构造POST表单Map
        Map<String, Object> paramsMap = InputLedgerParam.buildReimbursedv2();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 进项业务台账查询
     */
    @Test
    public void search() {
        apiToolVO.setUrl(inputLedgerUrlConfig.getSearch());
        //构造POST表单Map
        Map<String, Object> paramsMap = InputLedgerParam.buildSearch();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }


}
