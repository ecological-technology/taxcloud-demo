package com.yonyou.eco.taxclouddemo.input;

import com.yonyou.eco.taxclouddemo.config.InputTaxUrlConfig;
import com.yonyou.eco.taxclouddemo.param.input_ticket.StaBookBuildParam;
import com.yonyou.eco.taxclouddemo.service.ITaxCloudAPIToolService;
import com.yonyou.eco.taxclouddemo.utils.Base64Util;
import com.yonyou.eco.taxclouddemo.vo.APIToolVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

/**
 * 功能描述: 报销台账接口测试样例
 * @Author: jiaoguojin
 * @Date: 2023/3/8 15:35
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BillCollectionsTest {

    @Autowired
    APIToolVO apiToolVO;

    @Autowired
    InputTaxUrlConfig inputTaxUrlConfig;

    @Autowired
    ITaxCloudAPIToolService toolService;

    /*
     * OCR识别
     */
    @Test
    public void recognise() {
        apiToolVO.setUrl(inputTaxUrlConfig.getRecognise());
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.buildRecognisePostParam();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 报销台账增值税发票查验并缓存
     */
    @Test
    public void verify_and_save() {
        apiToolVO.setUrl(inputTaxUrlConfig.getVerify_and_save());
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.verfiy();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 增值税发票从缓存提交保存到报销台账
     */
    @Test
    public void submit() {
        apiToolVO.setUrl(inputTaxUrlConfig.getSubmit());
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.submit();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 报销台账发票上传返回全票面信息(PDF)
     * 支持pdf、ofd格式增值税发票，pdf格式通用电子发票到机打发票 (状态变更为报销中)
     */
    @Test
    public void uploadpdf() {
        apiToolVO.setUrl(inputTaxUrlConfig.getUploadpdf());
        Map<String, Object> paramsMap = StaBookBuildParam.V4_UPLOADPDF();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 纸票查验进报销台账
     */
    @Test
    public void verify_and_submit() {
        apiToolVO.setUrl(inputTaxUrlConfig.getVerify_and_submit());
        Map<String, Object> paramsMap = StaBookBuildParam.VERIFY_AND_SUBMIT();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 发票不需要查验进入报销台账
     */
    @Test
    public void ocr_save() {
        apiToolVO.setUrl(inputTaxUrlConfig.getOcr_save());
        Map<String, Object> paramsMap = StaBookBuildParam.OCR_Save();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 识别结果保存台帐
     */
    @Test
    public void direct_save() {
        apiToolVO.setUrl(inputTaxUrlConfig.getDirect_save());
        Map<String, Object> paramsMap = StaBookBuildParam.REIMBURSE_DIRECT_SAVE();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 全票种不查验进台账
     */
    @Test
    public void bill_direct_save() {
        apiToolVO.setUrl(inputTaxUrlConfig.getBill_direct_save());
        Map<String, Object> paramsMap = StaBookBuildParam.direct_save();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 报销台账报销
     */
    @Test
    public void reimbursed() {
        apiToolVO.setUrl(inputTaxUrlConfig.getReimbursed());
        Map<String, Object> paramsMap = StaBookBuildParam.reimbursed();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 台账取消报销
     */
    @Test
    public void cancel_reimbursed() {
        apiToolVO.setUrl(inputTaxUrlConfig.getCancel_reimbursed());
        Map<String, Object> paramsMap = StaBookBuildParam.cancelReimbursed();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 报销台账记账
     */
    @Test
    public void account() {
        apiToolVO.setUrl(inputTaxUrlConfig.getAccount());
        Map<String, Object> paramsMap = StaBookBuildParam.account();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 报销台账取消记账
     */
    @Test
    public void cancel_account() {
        apiToolVO.setUrl(inputTaxUrlConfig.getCancel_account());
        Map<String, Object> paramsMap = StaBookBuildParam.cancelAccount();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 报销台账删除
     */
    @Test
    public void delete() {
        apiToolVO.setUrl(inputTaxUrlConfig.getDelete());
        Map<String, Object> paramsMap = StaBookBuildParam.delete();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 报销台账查询附件(全票种)
     */
    @Test
    public void view_url() {
        apiToolVO.setUrl(inputTaxUrlConfig.getView_url());
        Map<String, Object> paramsMap = StaBookBuildParam.VIEW_URL();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 其他发票台账查询
     */
    @Test
    public void other() {
        apiToolVO.setUrl(inputTaxUrlConfig.getOther()+"?pagenum=1&pagesize=15");
        Map<String, Object> paramsMap = StaBookBuildParam.OTHER();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 飞机票台账查询
     */
    @Test
    public void air() {
        apiToolVO.setUrl(inputTaxUrlConfig.getAir()+"?pagenum=1&pagesize=15");
        Map<String, Object> paramsMap = StaBookBuildParam.find();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 火车票台账查询
     */
    @Test
    public void train() {
        apiToolVO.setUrl(inputTaxUrlConfig.getTrain()+"?pagenum=1&pagesize=15");
        Map<String, Object> paramsMap = StaBookBuildParam.find();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 出租车台账查询
     */
    @Test
    public void taxi() {
        apiToolVO.setUrl(inputTaxUrlConfig.getTaxi()+"?pagenum=1&pagesize=15");
        Map<String, Object> paramsMap = StaBookBuildParam.find();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 机打发票台账查询
     */
    @Test
    public void machine() {
        apiToolVO.setUrl(inputTaxUrlConfig.getMachine()+"?pagenum=1&pagesize=15");
        Map<String, Object> paramsMap = StaBookBuildParam.find();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 定额发票台账查询
     */
    @Test
    public void quota() {
        apiToolVO.setUrl(inputTaxUrlConfig.getQuota()+"?pagenum=1&pagesize=15");
        Map<String, Object> paramsMap = StaBookBuildParam.find();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 过路费发票台账查询
     */
    @Test
    public void tolls() {
        apiToolVO.setUrl(inputTaxUrlConfig.getTolls()+"?pagenum=1&pagesize=15");
        Map<String, Object> paramsMap = StaBookBuildParam.find();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 客运汽车发票台账查询
     */
    @Test
    public void passenger() {
        apiToolVO.setUrl(inputTaxUrlConfig.getPassenger()+"?pagenum=1&pagesize=15");
        Map<String, Object> paramsMap = StaBookBuildParam.find();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 增值税发票台账查询
     */
    @Test
    public void reimburse() {
        apiToolVO.setUrl(inputTaxUrlConfig.getReimburse()+"?pagenum=1&pagesize=15");
        Map<String, Object> paramsMap = StaBookBuildParam.find();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 报销台账查询详情信息
     */
    @Test
    public void detail() {
        apiToolVO.setUrl(inputTaxUrlConfig.getDetail());
        Map<String, Object> paramsMap = StaBookBuildParam.query();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 报销台账置支付状态
     */
    @Test
    public void paid() {
        apiToolVO.setUrl(inputTaxUrlConfig.getPaid());
        Map<String, Object> paramsMap = StaBookBuildParam.paid();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 报销台账取消支付状态
     */
    @Test
    public void cancel_paid() {
        apiToolVO.setUrl(inputTaxUrlConfig.getCancel_paid());
        Map<String, Object> paramsMap = StaBookBuildParam.canclePaid();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 报销台账更新凭证号
     */
    @Test
    public void update_voucherid() {
        apiToolVO.setUrl(inputTaxUrlConfig.getUpdate_voucherid());
        Map<String, Object> paramsMap = StaBookBuildParam.UPDATE_VOUCHERID();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 报销台账更新来源单据号
     */
    @Test
    public void update_srcbill() {
        apiToolVO.setUrl(inputTaxUrlConfig.getUpdate_srcbill());
        Map<String, Object> paramsMap = StaBookBuildParam.UPDATE_SRCBILL();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 报销台账设置项目
     */
    @Test
    public void project_update() {
        apiToolVO.setUrl(inputTaxUrlConfig.getProject_update());
        Map<String, Object> paramsMap = StaBookBuildParam.PROJECT_UPDATE();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

}

