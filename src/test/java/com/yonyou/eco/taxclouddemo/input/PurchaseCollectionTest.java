package com.yonyou.eco.taxclouddemo.input;

import com.yonyou.eco.taxclouddemo.config.InputTaxUrlConfig;
import com.yonyou.eco.taxclouddemo.param.input_ticket.PurchaseParam;
import com.yonyou.eco.taxclouddemo.param.input_ticket.StaBookBuildParam;
import com.yonyou.eco.taxclouddemo.param.personal_ticket_holder.ReimburseCollection;
import com.yonyou.eco.taxclouddemo.service.ITaxCloudAPIToolService;
import com.yonyou.eco.taxclouddemo.utils.Base64Util;
import com.yonyou.eco.taxclouddemo.vo.APIToolVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

/**
 * 功能描述: 采购台账接口测试样例
 * @Author: jiaoguojin
 * @Date: 2023/3/8 15:39
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PurchaseCollectionTest {

    @Autowired
    APIToolVO apiToolVO;

    @Autowired
    InputTaxUrlConfig inputTaxUrlConfig;

    @Autowired
    ITaxCloudAPIToolService toolService;

    /*
     * OCR识别
     */
    @Test
    public void recognise() {
        apiToolVO.setUrl(inputTaxUrlConfig.getRecognise());
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.buildRecognisePostParam();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 采购台账发票查验并缓存
     */
    @Test
    public void verify() {
        apiToolVO.setUrl(inputTaxUrlConfig.getVerify());
        Map<String, Object> paramsMap = PurchaseParam.PURCHASE_VERIFY();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 采购台账发票从缓存提交保存到采购台账
     */
    @Test
    public void save() {
        apiToolVO.setUrl(inputTaxUrlConfig.getEinvoice_save());
        Map<String, Object> paramsMap = PurchaseParam.PURCHASE_SAVE();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 采购台账发票上传
     */
    @Test
    public void uploadpdf() {
        apiToolVO.setUrl(inputTaxUrlConfig.getEinvoice_uploadpdf());
        Map<String, Object> paramsMap = ReimburseCollection.uploadpdf();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 不需要查验进入台账
     * 客户从第三方系统获得增值税发票全票面信息，不需要通过税务云再次查验获得全票面信息，直接通过接口传入台账
     */
    @Test
    public void purchase_direct_save() {
        apiToolVO.setUrl(inputTaxUrlConfig.getPurchase_direct_save());
        Map<String, Object> paramsMap = PurchaseParam.directSave();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 采购台账删除发票
     */
    @Test
    public void purchase_delete() {
        apiToolVO.setUrl(inputTaxUrlConfig.getEinvoice_delete());
        Map<String, Object> paramsMap = PurchaseParam.PURCHASE_DELETE();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 采购台账发票查询电票(此接口返回pdf)
     */
    @Test
    public void einvoice_query() {
        apiToolVO.setUrl(inputTaxUrlConfig.getEinvoice_query());
        Map<String, Object> paramsMap = PurchaseParam.EINVOICE_QUERY();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 采购台账查询（新）
     */
    @Test
    public void purchaseQuery() {
        apiToolVO.setUrl(inputTaxUrlConfig.getEinvoice_query());
        Map<String, Object> paramsMap = PurchaseParam.query();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 采购台账入账 入账=记账
     */
    @Test
    public void accountstatus() {
        apiToolVO.setUrl(inputTaxUrlConfig.getAccountStatus());
        Map<String, Object> paramsMap = PurchaseParam.ACCOUNTSTATUS();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 采购台账取消入账  更改购买发票状态为未记账状态
     */
    @Test
    public void cancelaccount() {
        apiToolVO.setUrl(inputTaxUrlConfig.getEinvoice_cancelAccount());
        Map<String, Object> paramsMap = PurchaseParam.CANCELACCOUNT();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 采购台账结算  结算不等于支付，结算后改变为已结算状态
     */
    @Test
    public void settle() {
        apiToolVO.setUrl(inputTaxUrlConfig.getSettle());
        Map<String, Object> paramsMap = ReimburseCollection.purchase();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 采购台账取消结算
     */
    @Test
    public void purchaseUnset() {
        apiToolVO.setUrl(inputTaxUrlConfig.getUnsettle());
        Map<String, Object> paramsMap = ReimburseCollection.purchase();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 采购台账附件查询 返回页面为列表，操作查看可以进行下载（有效时间为30分钟）
     */
    @Test
    public void purchase_view_ur() {
        apiToolVO.setUrl(inputTaxUrlConfig.getPurchase_view_url());
        Map<String, Object> paramsMap = PurchaseParam.PURCHASE_VIEW_URL();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 采购台账附件预览 只能看不能下载（有效时间为10分钟）
     */
    @Test
    public void preview_link() {
        apiToolVO.setUrl(inputTaxUrlConfig.getPreview_link());
        Map<String, Object> paramsMap = PurchaseParam.perview();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 采购台账更新凭证号接口
     */
    @Test
    public void updataVoucherid() {
        apiToolVO.setUrl(inputTaxUrlConfig.getUpdate_voucherid());
        Map<String, Object> paramsMap = PurchaseParam.updataVoucherid();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }

    /*
     * 采购台账更新来源单据号接口
     */
    @Test
    public void updataSrcbill() {
        apiToolVO.setUrl(inputTaxUrlConfig.getUpdate_srcbill());
        Map<String, Object> paramsMap = PurchaseParam.updataSrcbill();
        apiToolVO.setRequestMap(paramsMap);
        toolService.postByJson(apiToolVO);
    }
}
