package com.yonyou.eco.taxclouddemo.constant;

/**
 * @Description: 企业微信常量类
 * @Author: jiaogjin
 * @Date: 2022/5/27 11:35
 */
public class WeChatConstant {

    /** 获取访问用户身份 */
    public static final String GETUSERINFO = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo";

    /** 读取成员 */
    public static final String GETUSER = "https://qyapi.weixin.qq.com/cgi-bin/user/get";

    /** 获取企业token */
    public static final String GETTOKEN = "https://qyapi.weixin.qq.com/cgi-bin/gettoken";

    /** 获取企业的jsapi_ticket */
    public static final String GET_JSAPI_TICKET = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket";

    /** 微信企业密钥 */
    public static final String CORPSECRET = "zRgG4DWLNFNHT7MbC067yT0vMsec0qZ0pvAO8RrxH24";


}
