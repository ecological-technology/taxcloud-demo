package com.yonyou.eco.taxclouddemo.constant;

/**
 * @Description: 钉钉常量类
 * @Author: jiaogjin
 * @Date: 2022/5/27 11:35
 */
public class DingDingConstant {

    /** 应用appKey */
    public static final String APPKEY = "dingnvkrf5gacfqpimxe";

    /** 应用AppSecret */
    public static final String APPSECRET = "UnSNVKq2-BxSrH2YAhXMdXuYVkikoGFKDT03BBfHuswrcI11zFDWn8lkIu5QieBA";

    /** 获取企业token */
    public static final String GETTOKEN = "https://oapi.dingtalk.com/gettoken";

    /** 通过免登码获取用户信息 */
    public static final String GETUSERINFO = "https://oapi.dingtalk.com/topapi/v2/user/getuserinfo";

    /** 查询用户详情 */
    public static final String GETUSERDETAILINFO = "https://oapi.dingtalk.com/topapi/v2/user/get";

}
