package com.yonyou.eco.taxclouddemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaxcloudDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaxcloudDemoApplication.class, args);
    }

}
