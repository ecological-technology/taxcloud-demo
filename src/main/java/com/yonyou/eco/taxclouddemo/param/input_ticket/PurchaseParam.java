package com.yonyou.eco.taxclouddemo.param.input_ticket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: kw
 * @description: 采购台账
 * @create: 2021/03/02 10:20
 */
public class PurchaseParam {

    //采购台账查询接口（新）
    public static Map<String, Object> query() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        //paramsMap.put("fpHm", "044004193493");
        //paramsMap.put("fpDm", "27022641");
        paramsMap.put("xsfMc", "接口测试组织-请勿修改");
        //paramsMap.put("submitter", "提交人");
        //paramsMap.put("purchaser", "采购人");
        //paramsMap.put("submitDate_begin", "");
        //paramsMap.put("submitDate_end", "提交时期aaaaa-起始");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("nsrsbh", "201609140000001");
        //paramsMap.put("kprq_begin", "开票日期-起始");
        //paramsMap.put("kprq_end", "开票日期-结束");
        //paramsMap.put("verifyStatus", "");
        //paramsMap.put("fplx", "");
        paramsMap.put("hasItems", "true");
        paramsMap.put("settleStatus", 1);
        //从1开始
        paramsMap.put("page", 1);
        paramsMap.put("size", 5);
        return paramsMap;
    }

    //采购台账预览
    public static Map<String, Object> perview() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("billType", "invoice");
        paramsMap.put("billCode", "044001505111");
        paramsMap.put("billNum", "69305249");
        return paramsMap;
    }

    //不需要查验进入台账
    public static Map<String, Object> directSave() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "91440101080359573F");
        paramsMap.put("submitter", "8888");
        paramsMap.put("invoices", buildinvoices());
        return paramsMap;
    }
    //采购台账更新凭证号接口
    public static Map<String, Object> updataVoucherid() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("invoices", buildinvoices2());
        return paramsMap;
    }
    //采购台账更新来源单据号接口
    public static Map<String, Object> updataSrcbill() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("invoices", buildinvoices3());
        return paramsMap;
    }
    private static Object buildinvoices2() {
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("voucherid", "99999");
        data.put("fpDm", "044004193493");
        data.put("fpHm", "27022641");
        data.put("srcBillType", "tims-server");
        data.put("srcBillCode", "BX1905260023");
        datas.add(data);
        return datas;
    }
    private static Object buildinvoices3() {
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("srcBillCode", "9999999");
        data.put("srcBillType", "invoice");
        data.put("fpDm", "044004193493");
        data.put("fpHm", "27022641");
        datas.add(data);
        return datas;
    }

    private static Object buildinvoices() {
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("hjje", 1395.28);
        data.put("jshj", 1479.00);
        data.put("fpDm", "3100201130");
        data.put("kprq", "20210421");
        data.put("fpHm", "51143619");
        data.put("jym", "162191");
        data.put("srcBillType", "invoice");
        data.put("srcBillCode", "51143619");
        data.put("fplx", "4");
        data.put("gmfDzdh", "广州市黄埔区黄埔大道东856号（A-2）801-812室（仅限办公） 02062981357");
        data.put("gmfMc", "广东卓志供应链科技有限公司");
        data.put("gmfNsrsbh", "91440101080359573F");
        data.put("gmfYhzh", "中国银行股份有限公司广州港湾路支行 678261875314");
        data.put("xsfDzdh", "162191");
        data.put("xsfYhzh", "工商银行上海市虹口区武进路支行1001213909200188728");
        data.put("xsfMc", "上海潼季酒店管理有限公司");
        data.put("xsfNsrsbh", "91310109324291588H");
        data.put("items", buildItems());
        datas.add(data);
        return datas;
    }

    public static List<Object> buildItems() {
        List<Object> items = new ArrayList<Object>();
        Map<String, Object> item = new HashMap<String, Object>();
        //   "detailMotor":null,
        item.put("dw", "天");
        item.put("yhzcbs", 0);
        item.put("xmsl", 3);
        item.put("xmmc", "*住宿服务*住宿费");
        item.put("xmje", 1395.28);
        item.put("xmdj", 465.0933333333);
        item.put("sl", 0.060000);
        item.put("se", 83.72);
        item.put("flightNumber", "HU7678");
        items.add(item);
        return items;
    }

    /**
     * 功能描述: 采购查验并缓存发票  构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/21 11:40
     */
    public static Map<String, Object> PURCHASE_VERIFY() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("orgCode", "20160914001");
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("submitter", "jgj");
        paramsMap.put("invoices", buildPurchaseVerifyInvoices());
        return paramsMap;
    }

    /**
     * 功能描述:
     * @Author: jiaoguojin
     * @Date: 2022/6/21 13:45
     */
    private static Object buildPurchaseVerifyInvoices() {
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("fpDm", "153002209100");
        data.put("fpHm", "03494805");
        data.put("kprq", "2022-04-28");
        data.put("hjje", 773.00);
        data.put("jshj", 773.00);
        data.put("jym", "9e14d");
        data.put("srcapp", "invoice");
        data.put("srcBillType", "51143619");
        data.put("srcBillCode", "4");
        datas.add(data);
        return datas;
    }

    /**
     * 功能描述: 从缓存保存到企业采购台账
     * @Author: jiaoguojin
     * @Date: 2022/6/21 11:40
     */
    public static Map<String, Object> PURCHASE_SAVE() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("orgCode", "20160914001");
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("submitter", "jgj");
        paramsMap.put("invoices", buildPurchaseSaveInvoices());
        return paramsMap;
    }

    private static Object buildPurchaseSaveInvoices() {
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("fpDm", "153002209100");
        data.put("fpHm", "03494805");
        data.put("srcapp", "invoice");
        data.put("srcBillType", "51143619");
        data.put("srcBillCode", "4");
        data.put("saveToken", "9e14d214322343242342334");
        datas.add(data);
        return datas;
    }

    /**
     * 功能描述: 采购台账删除发票 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/21 11:40
     */
    public static Map<String, Object> PURCHASE_DELETE() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("fpDm", "111005180827");
        data.put("fpHm", "64156159");
        datas.add(data);
        paramsMap.put("invoices", datas);
        return paramsMap;
    }

    /**
     * 功能描述: 采购台账发票查询电票 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/21 11:41
     */
    public static Map<String, Object> EINVOICE_QUERY() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("hasItems", "false");
        paramsMap.put("hasPdf", "true");
        paramsMap.put("page", 1);
        paramsMap.put("size", 5);
        return paramsMap;
    }

    /**
     * 功能描述: 采购台账入账 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/21 11:41
     */
    public static Map<String, Object> ACCOUNTSTATUS() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("checkallerror", "1");
        paramsMap.put("invoices", buildAccountstatusInvoices());
        return paramsMap;
    }

    /**
     * 功能描述: 采购台账入账票据数组 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/21 14:34
     */
    private static Object buildAccountstatusInvoices() {
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("fpDm", "044004193493");
        data.put("fpHm", "27022641");
        data.put("voucherid", "123456789");
        data.put("accountUser", "jgj");
        data.put("accountNote", "ceshi");
        data.put("accountTime", "2022-06-21");
        data.put("accountPeriod", "202206");
        datas.add(data);
        return datas;
    }

    /**
     * 功能描述:  采购台账取消入账 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/21 11:41
     */
    public static Map<String, Object> CANCELACCOUNT() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("checkallerror", "1");
        paramsMap.put("invoices", buildCancelaccountinvoices());
        return paramsMap;
    }

    /**
     * 功能描述: 采购台账取消入账数组 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/21 14:43
     */
    private static Object buildCancelaccountinvoices() {
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("fpDm", "044004193493");
        data.put("fpHm", "27022641");
        datas.add(data);
        return datas;
    }

    /**
     * 功能描述:  采购台账附件查询 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/21 11:48
     */
    public static Map<String, Object> PURCHASE_VIEW_URL() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("fpDm", "044001505111");
        data.put("fpHm", "69305249");
        datas.add(data);
        Map<String, Object> data1 = new HashMap<String, Object>();
        data1.put("fpDm", "022007848737");
        data1.put("fpHm", "95464143");
        datas.add(data1);
        Map<String, Object> data2 = new HashMap<String, Object>();
        data2.put("fpDm", "233008807709");
        data2.put("fpHm", "40988614");
        datas.add(data2);
        paramsMap.put("invoices", datas);
        return paramsMap;
    }
}
