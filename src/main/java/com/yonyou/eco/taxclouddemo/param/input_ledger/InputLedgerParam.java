package com.yonyou.eco.taxclouddemo.param.input_ledger;

import org.apache.commons.lang3.ArrayUtils;

import java.util.*;

/**
 * @Description: 进项业务台账、票据中心接口测试参数
 * @Author: jiaoguojin
 * @Date: 2023/4/6 17:27
 */
public class InputLedgerParam {


    public static Map<String, Object> buildSaveonlytobillcenter() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("submitter", "jgj");
        paramsMap.put("busiFlag", "0");//业务标识	0- 报销，1-采购
        paramsMap.put("srcBillType", "jgj");//来源业务系统
        paramsMap.put("srcBillCode", "jgj");//业务系统单据号
        paramsMap.put("srcBillid", "jgj");//业务单据id
        paramsMap.put("bills", buildBills());
        return paramsMap;
    }

    private static Object buildBills() {
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("billType", "invoice");
        data.put("imageId", 1643882153398525952L);//ocr识别返回的imageId,传了它才有图片影响
        data.put("filePath", "");
        data.put("data", buildInvoice());
        datas.add(data);
        return datas;
    }

    private static Object buildInvoice() {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("hjje", 200.0);
        data.put("jshj", 200.0);
        data.put("fpDm", "011002300211");
        data.put("fpHm", "59978095");
        data.put("kprq", "20230403");
        data.put("jym", "00749720239600894076");
        data.put("fplx", "1");
        data.put("gmfDzdh", "");
        data.put("gmfMc", "用友网络科技股份有限公司");
        data.put("gmfNsrsbh", "91110000600001760P");
        data.put("gmfYhzh", "");
        data.put("xsfDzdh", "北京市西城区复兴门南大街6号,10010");
        data.put("xsfYhzh", "中国工商银行北京市长安支行0200003319210012727");
        data.put("xsfMc", "中国联合网络通信有限公司北京市分公司");
        data.put("xsfNsrsbh", "911100008016572721");
        data.put("items", buildItems());
        return  data;
    }

    private static Object buildItems() {
        List<Object> items = new ArrayList<Object>();
        Map<String, Object> item = new HashMap<String, Object>();
        //item.put("dw", "天");
        //item.put("xmsl", 3);
        item.put("xmmc", "*电信服务*通信服务费");
        item.put("xmje", 200.0);
        //item.put("xmdj", 465.0933333333);
        item.put("sl", 0);
        item.put("se", 0);
        items.add(item);
        return items;
    }

    public static Map<String, Object> buildSaveonlybusicollection() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("submitter", "jgj");
        paramsMap.put("busiFlag", "0");//业务标识	0- 报销，1-采购
        paramsMap.put("billDate", "2023-05-05");//业务单据制单日期
        paramsMap.put("srcBillType", "jgj");//来源业务系统
        paramsMap.put("srcBillCode", "jgj");//业务系统单据号
        paramsMap.put("srcBillid", "jgj");//业务单据id
        paramsMap.put("bills", buildBusicollectionBills());
        return paramsMap;
    }

    private static Object buildBusicollectionBills() {
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("billType", "invoice");
        data.put("reimburseMoney", 123.00);//支持部分报销，一票多报场景，但不能超过发票价税合计金额
        data.put("reimburseTax", 0.00);
        data.put("fpDm", "011002300211");
        data.put("fpHm", "59978095");
        datas.add(data);
        return datas;
    }

    public static Map<String, Object> buildQueryvatbilllist() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("searchParam", buildSearchParam());
        paramsMap.put("pagenum", 1);
        paramsMap.put("pagesize", 20);
        return paramsMap;
    }

    private static Object buildSearchParam() {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("orgcode", "");
        data.put("invoiceCode", "");
        data.put("invoiceNum", "");
        data.put("billDateBegin", "2023-04-01");//格式（yyyy-MM-dd）
        data.put("billDateEnd", "2023-04-01");//格式（yyyy-MM-dd）
        data.put("submitter", "");
        data.put("xsfMc", "");
        data.put("xsfNsrsbh", "");
        /*data.put("reimburseStatus", Arrays.asList(new String[] {""}));
        data.put("accountStatus", Arrays.asList(new String[] {""}));
        data.put("suspectFlag", Arrays.asList(new String[] {""}));
        data.put("suspectStatus", Arrays.asList(new String[] {""}));
        data.put("compareStatus", Arrays.asList(new String[] {""}));
        data.put("yyVerifyStatus", Arrays.asList(new String[] {""}));
        data.put("thirdVerifyStatus", Arrays.asList(new String[] {""}));
        data.put("signStatus", Arrays.asList(new String[] {""}));*/
        data.put("srcBilltype", "");
        data.put("srcBillcode", "");
        data.put("srcBillid", "");
        return data;
    }

    public static Map<String, Object> buildDelonlybusicollection() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("srcBillType", "jgj");//来源业务系统
        paramsMap.put("srcBillCode", "jgj");//业务系统单据号
        paramsMap.put("srcBillid", "jgj");//业务单据id
        paramsMap.put("bills", buildDelonlybusicollectionBills());
        return paramsMap;
    }

    private static Object buildDelonlybusicollectionBills() {
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("billType", "invoice");
        data.put("fpDm", "011002300211");
        data.put("fpHm", "59978095");
        datas.add(data);
        return datas;
    }

    public static Map<String, Object> buildReimbursedv2() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("srcBillType", "");//来源业务系统
        paramsMap.put("srcBillCode", "");//业务系统单据号
        paramsMap.put("srcBillid", "");//业务单据id
        paramsMap.put("reimburseUser", "jgj");//报账人
        paramsMap.put("reimburseDate", "2023-04-06");//报账时间（格式yyyy-MM-dd）
        paramsMap.put("vnote", "");//报账备注
        paramsMap.put("bills", buildDelonlybusicollectionBills());
        return paramsMap;
    }

    public static Map<String, Object> buildSearch() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("billType", "invoice");
        paramsMap.put("fpHm", "59978095");
        paramsMap.put("busiFlags", Arrays.asList(new String[] {"0"}));//业务类型(0:报销 1:采购)
        paramsMap.put("billDateStart", "");//业务单据制单开始日期(yyyy-MM-dd)
        paramsMap.put("billDateEnd", "");//业务单据制单结束日期(yyyy-MM-dd)
        paramsMap.put("submitDateStart", "");//登记开始日期(yyyy-MM-dd)
        paramsMap.put("submitDateEnd", "");//登记结束日期(yyyy-MM-dd)
        paramsMap.put("reimburseDateStart", "");//报账开始日期(yyyy-MM-dd)
        paramsMap.put("reimburseDateEnd", "");//报账终止日期(yyyy-MM-dd)
        paramsMap.put("accountTimeStart", "");//记账开始日期(yyyy-MM-dd)
        paramsMap.put("accountTimeEnd", "");//记账终止日期(yyyy-MM-dd)
        paramsMap.put("srcBilltype", "");//业务来源类型
        paramsMap.put("srcBillcode", "");//业务系统单据号
        paramsMap.put("srcBillid", "");//业务单据id
        paramsMap.put("page", buildPage());
        return paramsMap;
    }

    private static Object buildPage() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("page", 1);
        paramsMap.put("pageSize", 10);
        return paramsMap;
    }
}
