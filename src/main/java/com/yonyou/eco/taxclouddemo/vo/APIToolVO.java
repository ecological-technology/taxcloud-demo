package com.yonyou.eco.taxclouddemo.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @author: kw
 * @description: 税务云测试工具VO
 * @create: 2021/10/26 20:47
 */
@JsonSerialize
@Component
public class APIToolVO {

    private String domain;
    private String appid;
    private String certificate;
    private String password;
    private String url;
    private String requestBody;
    private Object requestMap;

    public String getDomain() {
        return domain.trim();
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getAppid() {
        return appid.trim();
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url.trim();
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public Object getRequestMap() {
        return requestMap;
    }

    public void setRequestMap(Object requestMap) {
        this.requestMap = requestMap;
    }

    @Override
    public String toString() {
        return "APIToolVO{" +
                "domain='" + domain + '\'' +
                ", appid='" + appid + '\'' +
                ", certificate='" + certificate + '\'' +
                ", password='" + password + '\'' +
                ", url='" + url + '\'' +
                ", requestBody='" + requestBody + '\'' +
                '}';
    }
}
