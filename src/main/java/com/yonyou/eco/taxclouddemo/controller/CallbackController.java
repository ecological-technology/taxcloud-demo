package com.yonyou.eco.taxclouddemo.controller;

import com.alibaba.fastjson.JSONObject;
import com.yonyou.eco.taxclouddemo.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Description: 回调接口写法
 * @Author: jiaoguojin
 * @Date: 2022/6/9 15:38
 */
@RestController
@Slf4j
@RequestMapping("/kaipiao")
public class CallbackController {

    /**
     * 功能描述: 第一种拿map接收
     * @Author: jiaoguojin
     * @Date: 2022/6/9 16:53
     */
    @ResponseBody
    @RequestMapping("/callback")
    public Result callback(@RequestBody Map<String,Object> map){
        log.info(map.toString());
        Result result = new Result();
        result.setCode("0000");
        result.setMsg("调用成功");
        return result;
    }

    /**
     * 功能描述: 第二种拿json介绍，注意必须是阿里巴巴json
     * @Author: jiaoguojin
     * @Date: 2022/6/9 16:54
     */
    @ResponseBody
    @RequestMapping("/callback1")
    public Result callback(@RequestBody JSONObject json){
        log.info(json.toString());
        Result result = new Result();
        result.setCode("0000");
        result.setMsg("调用成功");
        return result;
    }
}
