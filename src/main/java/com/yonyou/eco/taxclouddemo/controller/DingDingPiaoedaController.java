package com.yonyou.eco.taxclouddemo.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.dingtalkcontact_1_0.models.GetUserHeaders;
import com.aliyun.dingtalkoauth2_1_0.models.GetUserTokenRequest;
import com.aliyun.dingtalkoauth2_1_0.models.GetUserTokenResponse;
import com.aliyun.teaopenapi.models.Config;
import com.aliyun.teautil.models.RuntimeOptions;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.*;
import com.dingtalk.api.response.*;
import com.google.gson.JsonObject;
import com.yonyou.eco.taxclouddemo.constant.DingDingConstant;
import com.yonyou.eco.taxclouddemo.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;


/**
 * @Description: 钉钉对接个人票夹相关接口
 * @Author: jiaoguojin
 * @Date: 2022/6/10 14:05
 */
@RestController
@Slf4j
public class DingDingPiaoedaController {

    /**
     * 功能描述: 获取用户信息接口(接口一)
     *           钉钉参考文档地址：https://open.dingtalk.com/document/orgapp-server/enterprise-internal-application-logon-free
     * @Author: jiaoguojin
     * @Date: 2022/6/14 10:28
     */
    @GetMapping("/dingding/userinfo")
    public Result userinfo(@RequestParam String appid,
                           @RequestParam String code,
                           @RequestParam String ts,
                           @RequestParam String sign) throws Exception {
        log.info("请求参数为:appid="+appid+";code="+code+";ts="+ts+";sign="+sign);
        //接口返回对象
        Result result = new Result();
        //测试代码，省略了验签功能(根据文档《钉钉对接个人票夹技术方案.word》中签名说明提供的验签方式验证即可)。参考类本项目中的SignUtils

        //获取token
        DingTalkClient client = new DefaultDingTalkClient(DingDingConstant.GETTOKEN);
        OapiGettokenRequest req = new OapiGettokenRequest();
        req.setAppkey(DingDingConstant.APPKEY);
        req.setAppsecret(DingDingConstant.APPSECRET);
        req.setHttpMethod("GET");
        OapiGettokenResponse rsp = client.execute(req);
        JSONObject jsonObject = JSONObject.parseObject(rsp.getBody());
        String token = jsonObject.getString("access_token");
        log.info("token="+token);

        //获取用户信息
        DingTalkClient userClient = new DefaultDingTalkClient(DingDingConstant.GETUSERINFO);
        OapiV2UserGetuserinfoRequest reqUser = new OapiV2UserGetuserinfoRequest();
        reqUser.setCode(code);
        OapiV2UserGetuserinfoResponse userRsp = userClient.execute(reqUser, token);
        log.info("用户信息="+userRsp.getBody());
        JSONObject userJson = JSONObject.parseObject(userRsp.getBody());
        JSONObject userInfoJson = userJson.getJSONObject("result");
        String userId = userInfoJson.getString("userid");
        log.info("userId="+userId);

        //根据userId获取用户详情信息
        DingTalkClient userInfoClient = new DefaultDingTalkClient(DingDingConstant.GETUSERDETAILINFO);
        OapiV2UserGetRequest userInfoReq = new OapiV2UserGetRequest();
        userInfoReq.setUserid(userId);
        userInfoReq.setLanguage("zh_CN");
        OapiV2UserGetResponse userInfoRsp = userInfoClient.execute(userInfoReq, token);
        log.info("用户详情信息="+userInfoRsp.getBody());
        JSONObject userDetailInfoJson = JSONObject.parseObject(userInfoRsp.getBody());
        if(!("0").equals(userDetailInfoJson.getString("errcode"))){
            result.setCode("0001");
            result.setMsg("操作失败");
        }
        result.setCode("0000");
        result.setMsg("操作成功");
        HashMap<String,Object> map = new HashMap<String, Object>();
        map.put("email",userDetailInfoJson.getJSONObject("result").getString("email"));
        map.put("mobile",userDetailInfoJson.getJSONObject("result").getString("mobile"));
        map.put("name",userDetailInfoJson.getJSONObject("result").getString("name"));
        result.setDatas(map);
        log.info("接口返回信息："+JSONObject.toJSONString(result));
        return result;
    }

}
