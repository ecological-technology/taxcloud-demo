package com.yonyou.eco.taxclouddemo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author: kw
 * @description: 税务云开票URL配置读取类
 * @create: 2021/11/18 16:05
 */
@Data
@Configuration
@EnableConfigurationProperties(TaxCloudConfig.class)
@PropertySource(value = {"classpath:url.properties"},
        ignoreResourceNotFound = false, encoding = "UTF-8", name = "url.properties")
@ConfigurationProperties(prefix = OutputTaxUrlConfig.PREFIX, ignoreInvalidFields = true)
public class OutputTaxUrlConfig {
    public static final String PREFIX = "output-tax";

    // 签名验签
    private String testconnection;

    /*开票申请相关*/
    //开蓝票
    private String insertWithArray;
    //开蓝票-自动拆分
    private String insertWithSplit;
    //开票状态查询
    private String queryInvoiceStatus;
    //发票红冲
    private String red;
    //开票申请审核通过
    private String issue;
    //电子发票部分红冲
    private String part_red;
    //删除开票失败申请
    private String deleteInvoiceFailData;
    //重发邮件
    private String callBackByEmail;

    /*开票申请删除*/
    private String del;

    /*纸质发票打印*/
    private String print;

    /*请求二维码信息*/
    private String insertForQRInvoice;

    /*红字信息表*/
    //申请红字信息表（购方销方）
    private String insertWithRedApply;
    //查询红字信息表编号
    private String queryRedInfoApply;

    /*未开票收入管理*/
    //未开票查询
    private String result;
    //未开票管理新增单据
    private String save;
    //未开票记录变更查询
    private String changes;

    // 纸质发票作废
    private String invalid;


}
