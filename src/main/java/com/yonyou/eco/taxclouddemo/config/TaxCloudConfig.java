package com.yonyou.eco.taxclouddemo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author: kw
 * @description: 税务云环境配置类
 * @create: 2021/11/18 15:10
 */
@Data
@Configuration
@EnableConfigurationProperties(TaxCloudConfig.class)
@PropertySource(value = {"classpath:taxcloud-${spring.profiles.active}.properties"},
        ignoreResourceNotFound = false, encoding = "UTF-8")
@ConfigurationProperties(prefix = TaxCloudConfig.PREFIX, ignoreInvalidFields = true)
public class TaxCloudConfig {
    public static final String PREFIX = "taxcloud";

    //环境域名
    private String domain;

    //APPID
    private String appid;

    //证书路径
    private String certificate;

    //证书密码
    private String certificatepassword;
}
