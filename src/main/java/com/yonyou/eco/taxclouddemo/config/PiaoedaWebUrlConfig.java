package com.yonyou.eco.taxclouddemo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author: kw
 * @description: 税务云个人票夹URL置读取类
 * @create: 2021/12/17 20:58
 */
@Data
@Configuration
@EnableConfigurationProperties(TaxCloudConfig.class)
@PropertySource(value = {"classpath:url.properties"},
        ignoreResourceNotFound = false, encoding = "UTF-8", name = "url.properties")
@ConfigurationProperties(prefix = PiaoedaWebUrlConfig.PREFIX, ignoreInvalidFields = true)
public class PiaoedaWebUrlConfig {

    public static final String PREFIX = "piaoeda-web";

    // OCR识别接口
    private String rqrecognise;
    // OCR识别接口V2
    private String v2_rqrecognise;
    // OCR接口图片预览接口
    private String ocr_preview;
    // 个人票夹列表查询
    private String query;
    // 个人票夹票据详情查询
    private String detail;
    // 根据号码代码获取信息
    private String summary;
    // 修改发票状态
    private String purchaser_status;
    // 个人票夹附件预览
    private String preview;
    // 个人票夹行程单预览
    private String itinerary_preview;
    // 个人票夹行程单下载
    private String itinerary_download;
    // 个人票夹附件下载
    private String download;
    // 个人票夹新增
    private String add;
    // 个人票夹删除
    private String delete ;
    // 个人票夹修改
    private String update ;
    // 个人票夹提交至企业台账
    private String commit ;
    // 个人票夹发票提交到采购台账
    private String fetchPurchase;

}

