package com.yonyou.eco.taxclouddemo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author: kw
 * @description: 税务云受票URL配置读取类
 * @create: 2021/11/19 14:08
 */
@Data
@Configuration
@EnableConfigurationProperties(TaxCloudConfig.class)
@PropertySource(value = {"classpath:url.properties"},
        ignoreResourceNotFound = false, encoding = "UTF-8", name = "url.properties")
@ConfigurationProperties(prefix = InputTaxUrlConfig.PREFIX, ignoreInvalidFields = true)
public class InputTaxUrlConfig {

    public static final String PREFIX = "input-tax";

    //OCR识别
    private String recognise;

    //报销台账增值税发票查验并缓存
    private String verify_and_save;
    //增值税发票从缓存保存到报销台账
    private String submit;
    //报销台账发票上传返回全票面信息(PDF)
    private String uploadpdf;
    //纸票查验进报销台账
    private String verify_and_submit;
    //不需要查验进入台账
    private String direct_save;
    //识别结果保存台账                 
    private String ocr_save;
    //全票种不查验进台账
    private String bill_direct_save;
    //报销台账报销
    private String reimbursed;
    //报销台账取消报销                 
    private String cancel_reimbursed;
    //报销台账记账                     
    private String account;
    //报销台账取消记账                 
    private String cancel_account;
    //报销台账删除                     
    private String delete;
    //报销台账查询附件(全票种)         
    private String view_url;
    //其他发票台账查询                 
    private String other;
    //飞机票台账查询                   
    private String air;
    //火车票台账查询                   
    private String train;
    //出租车台账查询                   
    private String taxi;
    //机打发票台账查询                 
    private String machine;
    //定额发票台账查询                 
    private String quota;
    //过路费发票台账查询               
    private String tolls;
    //客运汽车发票台账查询             
    private String passenger;
    //增值税发票台账查询               
    private String reimburse;
    //报销台账查询详情信息             
    private String detail;
    //报销台账置支付状态               
    private String paid;
    //报销台账取消支付状态             
    private String cancel_paid;
    //报销台账更新凭证号接口           
    private String update_voucherid;
    //报销台账更新来源单据号           
    private String update_srcbill;
    //报销台账设置项目
    private String project_update;

    //采购台账查验并缓存
    private String verify;
    //采购发票从缓存保存到采购台账
    private String einvoice_save;
    //采购台账发票上传                 
    private String einvoice_uploadpdf;
    //采购台账不需要查验进台账
    private String purchase_direct_save;
    //采购台账发票删除
    private String einvoice_delete;
    //采购台账查询                     
    private String einvoice_query;
    //采购台账入账                     
    private String accountStatus;
    //采购台账取消入账                 
    private String einvoice_cancelAccount;
    //采购台账附件预览                 
    private String link;
    //采购台账查询附件                 
    private String einvoice_view_url;
    //采购台账结算                     
    private String settle;
    //采购台账结算                     
    private String unsettle;
    //采购台账附件查询
    private String purchase_view_url;
    //采购台账附件预览
    private String preview_link;
    //采购台账更新凭证号
    private String einvoice_update_voucherid;
    //采购台账更新来源单据号
    private String einvoice_update_srcbill;

}
