package com.yonyou.eco.taxclouddemo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 功能描述: 进项业务台账、票据中心接口url
 * @Author: jiaoguojin
 * @Date: 2023/4/6 17:00
 */
@Data
@Configuration
@EnableConfigurationProperties(TaxCloudConfig.class)
@PropertySource(value = {"classpath:url.properties"},
        ignoreResourceNotFound = false, encoding = "UTF-8", name = "url.properties")
@ConfigurationProperties(prefix = InputLedgerUrlConfig.PREFIX, ignoreInvalidFields = true)
public class InputLedgerUrlConfig {
    public static final String PREFIX = "input-tax-merge";

    /*进项业务台账*/
    //保存电子票据中心的票据到台账
    private String saveonlybusicollection;
    //删除台账（不包含票据删除）
    private String delonlybusicollection;
    //完成报账(支持按来源id报账)
    private String reimbursedv2;
    //进项业务台账查询
    private String search;


    /*进项电子票据*/
    //按号码代码查询票据信息
    private String query;
    //保存到进项电子票据中心
    private String saveonlytobillcenter;
    //完成报账(支持按来源id报账)
    private String queryvatbilllist;

}
