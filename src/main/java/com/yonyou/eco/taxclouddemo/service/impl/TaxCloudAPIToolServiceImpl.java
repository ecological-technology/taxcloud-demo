package com.yonyou.eco.taxclouddemo.service.impl;

import com.alibaba.fastjson.JSON;
import com.yonyou.eco.taxclouddemo.config.TaxCloudConfig;
import com.yonyou.eco.taxclouddemo.crypto.SignHelper;
import com.yonyou.eco.taxclouddemo.service.ITaxCloudAPIToolService;
import com.yonyou.eco.taxclouddemo.utils.*;
import com.yonyou.eco.taxclouddemo.vo.APIToolVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: kw
 * @description: 税务云api测试工具service实现类
 * @create: 2021/10/26 16:41
 */
@Slf4j
@Service
public class TaxCloudAPIToolServiceImpl implements ITaxCloudAPIToolService {

    @Autowired
    TaxCloudConfig taxCloudConfig;

    /**
     * 请求参数格式为：application/json; charset=UTF-8
     * @param apiToolVO
     */
    @Override
    public void postByJson(APIToolVO apiToolVO) {

        // 设置默认环境，读取的是taxcloud-*.properties
        setDefaultEnvironmentParam(apiToolVO);

        // 获取请求url拼接appid
        String uri = apiToolVO.getDomain() + apiToolVO.getUrl();
        if (uri.contains("?")) {
            uri += "&appid=" + apiToolVO.getAppid();
        } else {
            uri += "?appid=" + apiToolVO.getAppid();
        }

        // 获取请求body
        Map<String,Object> paramMap = (Map)apiToolVO.getRequestMap();

        String responseResult = "", curl = "";
        try {
            // 获取签名
            String sign = SignHelper.sign(paramMap, apiToolVO);
            // 返回结果
            responseResult = HttpClientUtil.jsonPost(uri, sign, paramMap);
            // 请求curl，用于导入postman工具联调
            curl = HttpToCurlUtils.postJsonCurl(sign, uri, JSON.toJSONString(paramMap));
        } catch (Exception e) {
            responseResult = e.getMessage();
            log.error(e.getMessage());
        }

        //日志输出
        log.info("CURL:"+"\n"+curl);
        log.info("请求url："+uri);
        log.info("请求参数："+JSON.toJSONString(paramMap));
        log.info("返回结果："+responseResult);

    }

    /**
     * 请求参数格式为：application/x-www-form-urlencoded; charset=UTF-8
     * @param apiToolVO
     */
    @Override
    public void postByForm(APIToolVO apiToolVO) {
        // 设置默认环境，读取的是taxcloud-*.properties
        setDefaultEnvironmentParam(apiToolVO);

        // 获取请求url拼接appid
        String uri = apiToolVO.getDomain() + apiToolVO.getUrl();
        if (uri.contains("?")) {
            uri += "&appid=" + apiToolVO.getAppid();
        } else {
            uri += "?appid=" + apiToolVO.getAppid();
        }

        // 获取表单请求参数
        Map<String,Object> paramMap = (Map)apiToolVO.getRequestMap();

        String responseResult = "";
        try {
            // 获取签名
            String sign = SignHelper.sign(paramMap, apiToolVO);
            // 返回结果
            responseResult = HttpClientUtil.post(uri, sign, paramMap);
        } catch (Exception e) {
            responseResult = e.getMessage();
            log.error(e.getMessage());
        }

        //日志输出
        log.info("返回结果："+responseResult);
    }

    /**
     * get请求方式
     * @param apiToolVO
     */
    @Override
    public void get(APIToolVO apiToolVO) {

        // 设置默认环境，读取的是taxcloud-*.properties
        setDefaultEnvironmentParam(apiToolVO);

        // 获取请求url拼接appid
        String uri = apiToolVO.getDomain() + apiToolVO.getUrl();
        if (uri.contains("?")) {
            uri += "&appid=" + apiToolVO.getAppid();
        } else {
            uri += "?appid=" + apiToolVO.getAppid();
        }

        // 获取请求body
        Map<String,Object> paramMap = (Map)apiToolVO.getRequestMap();

        String responseResult = "";
        try {
            // 获取签名
            String sign = SignHelper.sign(new HashMap(), apiToolVO);
            responseResult = HttpClientUtil.get(uri, sign, paramMap);
        } catch (Exception e) {
            responseResult = e.getMessage();
            log.error(e.getMessage());
        }

        //日志输出
        log.info("返回结果："+responseResult);

    }

    /**
     * 请求参数格式为：application/json; charset=UTF-8
     * @param apiToolVO
     */
    @Override
    public void postByJsonList(APIToolVO apiToolVO) {
        // 设置默认环境，读取的是taxcloud-*.properties
        setDefaultEnvironmentParam(apiToolVO);

        // 获取请求url拼接appid
        String uri = apiToolVO.getDomain() + apiToolVO.getUrl();
        if (uri.contains("?")) {
            uri += "&appid=" + apiToolVO.getAppid();
        } else {
            uri += "?appid=" + apiToolVO.getAppid();
        }

        // 获取请求body
        List paramList = (List)apiToolVO.getRequestMap();

        String responseResult = "", curl = "";
        try {
            // 获取签名
            String sign = SignHelper.sign(new HashMap<>(), apiToolVO);
            // 返回结果
            responseResult = HttpClientUtil.jsonPost(uri, sign, paramList);
            // 请求curl，用于导入postman工具联调
            curl = HttpToCurlUtils.postJsonCurl(sign, uri, JSON.toJSONString(paramList));
        } catch (Exception e) {
            responseResult = e.getMessage();
            log.error(e.getMessage());
        }

        //日志输出
        log.info("CURL:"+"\n"+curl);
        log.info("请求url："+uri);
        log.info("请求参数："+JSON.toJSONString(paramList));
        log.info("返回结果："+responseResult);
    }

    /*
     * @description:  设置默认环境参数
     * @author: kw
     * @date: 2021/11/18
     */
    APIToolVO setDefaultEnvironmentParam(APIToolVO apiToolVO) {
        apiToolVO.setDomain(taxCloudConfig.getDomain());
        apiToolVO.setAppid(taxCloudConfig.getAppid());
        apiToolVO.setCertificate(taxCloudConfig.getCertificate());
        apiToolVO.setPassword(taxCloudConfig.getCertificatepassword());
        return apiToolVO;
    }
}
