package com.yonyou.eco.taxclouddemo.service;

import com.yonyou.eco.taxclouddemo.vo.APIToolVO;

/**
 * @description:  税务云api测试工具service
 * @author: kw
 * @date: 2021/10/26
 */
public interface ITaxCloudAPIToolService {

    //发起post-json请求(参数为Map)
    void postByJson(APIToolVO apiToolVO);

    //发起post-form请求
    void postByForm(APIToolVO apiToolVO);

    //发起get请求
    void get(APIToolVO apiToolVO);

    //发起post-json请求(参数为List)
    void postByJsonList(APIToolVO apiToolVO);
}
