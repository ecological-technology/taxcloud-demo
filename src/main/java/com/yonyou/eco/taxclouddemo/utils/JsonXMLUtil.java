package com.yonyou.eco.taxclouddemo.utils;

import com.alibaba.fastjson.JSON;

import java.util.Map;

/**
 * @program:
 * @description: json数据转化的工具类
 * @author: kw
 * @create: 2020/04/14 03:20
 */
public class JsonXMLUtil {
    public static String obj2json(Object obj) throws Exception {
        return JSON.toJSONString(obj);
    }

    public static <T> T json2obj(String jsonStr, Class<T> clazz) throws Exception {
        return JSON.parseObject(jsonStr, clazz);
    }

    public static <T> Map<String, Object> json2map(String jsonStr) throws Exception {
        return JSON.parseObject(jsonStr, Map.class);
    }

    public static <T> T map2obj(Map<?, ?> map, Class<T> clazz) throws Exception {
        return JSON.parseObject(JSON.toJSONString(map), clazz);
    }

    public static <T> T json2array(String jsonStr) throws Exception {
        return (T) JSON.parseArray(jsonStr);
    }
}
