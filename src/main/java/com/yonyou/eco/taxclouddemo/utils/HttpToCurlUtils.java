package com.yonyou.eco.taxclouddemo.utils;

/**
 * @author kw
 * @description 将请求组装成curl，可和postman工具进行联调
 * @date 2021/10/28 13:59
 */
public class HttpToCurlUtils {

    public static String postJsonCurl(String sign, String url, String param){
        return "curl -X POST -H \"sign:"+sign+"\" -H \"Content-Type:application/json; charset=utf-8\"  -H \"Connection:Keep-Alive\" -H \"Accept-Encoding:gzip\" -H \"User-Agent:okhttp/4.9.0\" -d '"+param +"' \""+url+"\"";
    }
    public static String httpToCurlPost(String sign, String url){
        return "curl -X POST -H \"sign:"+sign+"\" -H \"Content-Type:application/x-www-form-urlencoded\"  -H \"Connection:Keep-Alive\" -H \"Accept-Encoding:gzip\" -H \"User-Agent:okhttp/4.9.0\" "+" \""+url+"\" -d '";
    }
    public static String httpToCurlGet(String sign, String url){
        return "curl -X GET -H \"sign:"+sign+"\"  -H \"User-Agent:okhttp/4.9.0\" "+" \""+url+"\" ";
    }

    public static String postFormCurl(String sign, String url, String param){
        return "curl -X POST -H \"sign:"+sign+"\" -H \"Content-Type:application/x-www-form-urlencoded; charset=utf-8\"  -H \"Connection:Keep-Alive\" -H \"Accept-Encoding:gzip\" -H \"User-Agent:okhttp/4.9.0\" -d '"+ param +"' \""+url+"\"";
    }
}
